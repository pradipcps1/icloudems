## INSTALL

- composer install
- cp .env.example .env
- php artisan key:generate
- php artisan migrate:refresh --seed
- php artisan app:zip-import
- php artisan app:temp-import


## Question 1
# Write the query to count the number of record in the parent table(either commonfeecollection or financial trans table)
- SELECT COUNT(`id`) AS `count` FROM `temp_data`;
- SELECT COUNT(`id`) AS `count` FROM `financial_trans`;
- SELECT COUNT(`id`) AS `count` FROM `common_fee_collections`;
- SELECT COUNT(`id`) AS `total` FROM `financial_tran_details`;
- SELECT COUNT(`id`) AS `total` FROM `common_fee_collection_headwises`;
- 884950
- 268435
- 268587
- 472729
- 412221

## due
- SELECT SUM(`due_amount`) AS `total` FROM `temp_data`;
- SELECT SUM(`amount`) AS `amount` FROM `financial_trans` WHERE `entrymode_no` = 0;
- SELECT SUM(`financial_tran_details`.`amount`) AS `amount`
    FROM `financial_tran_details` 
    INNER JOIN `financial_trans` ON `financial_trans`.`id` = `financial_tran_details`.`financial_tran_id` 
    WHERE `financial_trans`.`entrymode_no` = 0;
- 12654422921.00

- SELECT 
        `temp_data`.`id`, 
        `temp_data`.`date`, 
        `temp_data`.`admno`, 
        GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
        SUM(`temp_data`.`due_amount`) AS `due_amount`, 
        `financial_trans`.`amount` 
    FROM `financial_trans` 
    LEFT JOIN `temp_data` 
        ON `temp_data`.`date` = `financial_trans`.`tran_date` 
        AND `temp_data`.`admno` = `financial_trans`.`admno` 
        AND `temp_data`.`academic_year` = `financial_trans`.`acad_year`
    WHERE `financial_trans`.`entrymode_no` = 0 
        AND `temp_data`.`voucher_type` = 'DUE'
    GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`academic_year` 
    HAVING SUM(`temp_data`.`due_amount`) != `financial_trans`.`amount`;

## concession
- SELECT SUM(`concession_amount`) AS `total` FROM `temp_data`;
- SELECT SUM(`amount`) AS `amount` FROM `financial_trans` WHERE `entrymode_no` = 15 AND `financial_trans`.`type_concession` = 1;
- SELECT SUM(`financial_tran_details`.`amount`) AS `amount` 
    FROM `financial_tran_details` 
    INNER JOIN `financial_trans` ON `financial_trans`.`id` = `financial_tran_details`.`financial_tran_id` 
    WHERE `financial_trans`.`entrymode_no` = 15 
        AND `financial_trans`.`type_concession` = 1;
- 90544480.00

- SELECT 
        `temp_data`.`id`, 
        `temp_data`.`date`, 
        `temp_data`.`admno`, 
        GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
        SUM(`temp_data`.`concession_amount`) AS `concession_amount`, 
        `financial_trans`.`amount` 
    FROM `financial_trans` 
    LEFT JOIN `temp_data` 
        ON `temp_data`.`date` = `financial_trans`.`tran_date` 
        AND `temp_data`.`admno` = `financial_trans`.`admno` 
        AND `temp_data`.`academic_year` = `financial_trans`.`acad_year`
    WHERE `financial_trans`.`entrymode_no` = 15 
        AND `financial_trans`.`type_concession` = 1
        AND `temp_data`.`voucher_type` = 'CONCESSION'
    GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`academic_year` 
    HAVING SUM(`temp_data`.`concession_amount`) != `financial_trans`.`amount`;

## scholarship
- SELECT SUM(`scholarship_amount`) AS `total` FROM `temp_data`;
- SELECT SUM(`amount`) AS `amount` FROM `financial_trans` WHERE `entrymode_no` = 15 AND `financial_trans`.`type_concession` = 2;
- SELECT SUM(`financial_tran_details`.`amount`) AS `amount` 
    FROM `financial_tran_details` 
    INNER JOIN `financial_trans` ON `financial_trans`.`id` = `financial_tran_details`.`financial_tran_id` 
    WHERE `financial_trans`.`entrymode_no` = 15 
        AND `financial_trans`.`type_concession` = 2;
- 471818093.00

- SELECT 
        `temp_data`.`id`, 
        `temp_data`.`date`, 
        `temp_data`.`admno`, 
        GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
        SUM(`temp_data`.`scholarship_amount`) AS `scholarship_amount`, 
        `financial_trans`.`amount` 
    FROM `financial_trans` 
    LEFT JOIN `temp_data` 
        ON `temp_data`.`date` = `financial_trans`.`tran_date` 
        AND `temp_data`.`admno` = `financial_trans`.`admno` 
        AND `temp_data`.`academic_year` = `financial_trans`.`acad_year`
    WHERE `financial_trans`.`entrymode_no` = 15 
        AND `financial_trans`.`type_concession` = 2
        AND `temp_data`.`voucher_type` = 'SCHOLARSHIP'
    GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`academic_year` 
    HAVING SUM(`temp_data`.`scholarship_amount`) != `financial_trans`.`amount`;

## reverse_concession
- SELECT SUM(`reverse_concession_amount`) AS `total` FROM `temp_data`;
- SELECT SUM(`amount`) FROM `financial_trans` WHERE `entrymode_no` = 16;
- SELECT SUM(`financial_tran_details`.`amount`) AS `amount` 
    FROM `financial_tran_details` 
    INNER JOIN `financial_trans` ON `financial_trans`.`id` = `financial_tran_details`.`financial_tran_id` 
    WHERE `financial_trans`.`entrymode_no` = 16;
- 496960.00

- SELECT 
        `temp_data`.`id`, 
        `temp_data`.`date`, 
        `temp_data`.`admno`, 
        GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
        SUM(`temp_data`.`reverse_concession_amount`) AS `reverse_concession_amount`, 
        `financial_trans`.`amount` 
    FROM `financial_trans` 
    LEFT JOIN `temp_data` 
        ON `temp_data`.`date` = `financial_trans`.`tran_date` 
        AND `temp_data`.`admno` = `financial_trans`.`admno` 
        AND `temp_data`.`academic_year` = `financial_trans`.`acad_year`
    WHERE `financial_trans`.`entrymode_no` = 16 
        AND `temp_data`.`voucher_type` IN ('REVSCHOLARSHIP', 'REVCONCESSION')
    GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`academic_year` 
    HAVING SUM(`temp_data`.`reverse_concession_amount`) != `financial_trans`.`amount`;

## write_off
- SELECT SUM(`write_off_amount`) AS `total` FROM `temp_data`;
- SELECT SUM(`amount`) AS `amount` FROM `financial_trans` WHERE `entrymode_no` = 12;
- SELECT SUM(`financial_tran_details`.`amount`) AS `amount`
    FROM `financial_tran_details` 
    INNER JOIN `financial_trans` ON `financial_trans`.`id` = `financial_tran_details`.`financial_tran_id` 
    WHERE `financial_trans`.`entrymode_no` = 12;
- 784403968.60

- SELECT 
        `temp_data`.`id`, 
        `temp_data`.`date`, 
        `temp_data`.`admno`, 
        GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
        SUM(`temp_data`.`write_off_amount`) AS `write_off_amount`, 
        `financial_trans`.`amount` 
    FROM `financial_trans` 
    LEFT JOIN `temp_data` 
        ON `temp_data`.`date` = `financial_trans`.`tran_date` 
        AND `temp_data`.`admno` = `financial_trans`.`admno` 
        AND `temp_data`.`academic_year` = `financial_trans`.`acad_year`
    WHERE `financial_trans`.`entrymode_no` = 12 
        AND `temp_data`.`voucher_type` = 'REVDUE'
    GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`academic_year` 
    HAVING SUM(`temp_data`.`write_off_amount`) != `financial_trans`.`amount`;


## paid
- SELECT SUM(`paid_amount`) AS `total` FROM `temp_data`;
- SELECT SUM(`amount`) FROM `common_fee_collections` WHERE `entrymode_no` = 0;
- SELECT SUM(`common_fee_collection_headwises`.`amount`) AS `amount` 
    FROM `common_fee_collection_headwises` 
    INNER JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id` 
    WHERE `common_fee_collections`.`entrymode_no` = 0;
- 11461021901.40

    - RCPT
        - SELECT SUM(`common_fee_collection_headwises`.`amount`) AS `amount` 
            FROM `common_fee_collection_headwises` 
            INNER JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id` 
            WHERE `common_fee_collections`.`entrymode_no` = 0 
                AND `common_fee_collections`.`inactive` = 0;
        - 11441611278.40

        - SELECT 
                `temp_data`.`id`, 
                `temp_data`.`date`, 
                `temp_data`.`admno`, 
                `temp_data`.`roll_no`, 
                GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
                `temp_data`.`voucher_type`,
                SUM(`temp_data`.`paid_amount`) AS `paid_amount`, 
                `common_fee_collections`.`amount` 
            FROM `common_fee_collections` 
            LEFT JOIN `temp_data` 
                ON `temp_data`.`date` = `common_fee_collections`.`paid_date` 
                AND `temp_data`.`admno` = `common_fee_collections`.`admno` 
                AND `temp_data`.`roll_no` = `common_fee_collections`.`rollno` 
                AND `temp_data`.`academic_year` = `common_fee_collections`.`acadamic_year`
            WHERE `common_fee_collections`.`entrymode_no` = 0 
                AND `common_fee_collections`.`inactive` = 0
                AND `temp_data`.`voucher_type` = 'RCPT'
            GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`roll_no`, `temp_data`.`academic_year` 
            HAVING SUM(`temp_data`.`paid_amount`) != `common_fee_collections`.`amount`;

    - REVRCPT
        - SELECT SUM(`common_fee_collection_headwises`.`amount`) AS `amount` 
            FROM `common_fee_collection_headwises` 
            INNER JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id` 
            WHERE `common_fee_collections`.`entrymode_no` = 0 
                AND `common_fee_collections`.`inactive` = 1;
        - 19410623.00

        - SELECT 
                `temp_data`.`id`, 
                `temp_data`.`date`, 
                `temp_data`.`admno`, 
                `temp_data`.`roll_no`, 
                GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
                SUM(`temp_data`.`paid_amount`) AS `paid_amount`, 
                `common_fee_collections`.`amount` 
            FROM `common_fee_collections` 
            LEFT JOIN `temp_data` 
                ON `temp_data`.`date` = `common_fee_collections`.`paid_date` 
                AND `temp_data`.`admno` = `common_fee_collections`.`admno` 
                AND `temp_data`.`roll_no` = `common_fee_collections`.`rollno` 
                AND `temp_data`.`academic_year` = `common_fee_collections`.`acadamic_year`
            WHERE `common_fee_collections`.`entrymode_no` = 0 
                AND `common_fee_collections`.`inactive` = 1
                AND `temp_data`.`voucher_type` = 'REVRCPT'
            GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`roll_no`, `temp_data`.`academic_year` 
            HAVING SUM(`temp_data`.`paid_amount`) != `common_fee_collections`.`amount`;

## refund
- SELECT SUM(`refund_amount`) AS `total` FROM `temp_data`;
- SELECT SUM(`amount`) FROM `common_fee_collections` WHERE `entrymode_no` = 1 AND `inactive` IS NOT NULL;
- SELECT SUM(`common_fee_collection_headwises`.`amount`) AS `amount` 
    FROM `common_fee_collection_headwises` 
    INNER JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id` 
    WHERE `common_fee_collections`.`entrymode_no` = 1 
        AND `common_fee_collections`.`inactive` IS NOT NULL;
- -173381473.00
    
    - PMT
        - SELECT SUM(`common_fee_collection_headwises`.`amount`) AS `amount` 
            FROM `common_fee_collection_headwises` 
            INNER JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id` 
            WHERE `common_fee_collections`.`entrymode_no` = 1 
                AND `common_fee_collections`.`inactive` = 0;
        - -173249073.00

        - SELECT 
                GROUP_CONCAT(`temp_data`.`id`) AS `id`, 
                `temp_data`.`date`, 
                `temp_data`.`admno`, 
                `temp_data`.`roll_no`, 
                GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
                SUM(`temp_data`.`refund_amount`) AS `refund_amount`, 
                GROUP_CONCAT(`common_fee_collections`.`id`) AS `cid`, 
                `common_fee_collections`.`amount` 
            FROM `common_fee_collections` 
            RIGHT JOIN `temp_data` 
                ON `temp_data`.`date` = `common_fee_collections`.`paid_date` 
                AND `temp_data`.`admno` = `common_fee_collections`.`admno` 
                AND `temp_data`.`academic_year` = `common_fee_collections`.`acadamic_year`
            WHERE `common_fee_collections`.`entrymode_no` = 1 
                AND `common_fee_collections`.`inactive` = 0
                AND `temp_data`.`voucher_type` = 'PMT'
            GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`academic_year` 
            HAVING SUM(`temp_data`.`refund_amount`) != `common_fee_collections`.`amount`;
    
    - REVPMT
        - SELECT SUM(`common_fee_collection_headwises`.`amount`) AS `amount` 
            FROM `common_fee_collection_headwises` 
            INNER JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id` 
            WHERE `common_fee_collections`.`entrymode_no` = 1 
                AND `common_fee_collections`.`inactive` = 0;
        - -132400.00
        
        - SELECT 
                GROUP_CONCAT(`temp_data`.`id`) AS `id`, 
                `temp_data`.`date`, 
                `temp_data`.`admno`, 
                `temp_data`.`roll_no`, 
                GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
                SUM(`temp_data`.`refund_amount`) AS `refund_amount`, 
                GROUP_CONCAT(`common_fee_collections`.`id`) AS `cid`, 
                `common_fee_collections`.`amount` 
            FROM `common_fee_collections` 
            RIGHT JOIN `temp_data` 
                ON `temp_data`.`date` = `common_fee_collections`.`paid_date` 
                AND `temp_data`.`admno` = `common_fee_collections`.`admno` 
                AND `temp_data`.`academic_year` = `common_fee_collections`.`acadamic_year`
            WHERE `common_fee_collections`.`entrymode_no` = 1 
                AND `common_fee_collections`.`inactive` = 1
                AND `temp_data`.`voucher_type` = 'REVPMT'
            GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`academic_year` 
            HAVING SUM(`temp_data`.`refund_amount`) != `common_fee_collections`.`amount`;

## adjusted
- SELECT SUM(`adjusted_amount`) AS `total` FROM `temp_data`;
- SELECT SUM(`amount`) FROM `common_fee_collections` WHERE `entrymode_no` = 14;
- SELECT SUM(`common_fee_collection_headwises`.`amount`) AS `amount` 
    FROM `common_fee_collection_headwises` 
    INNER JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id` 
    WHERE `common_fee_collections`.`entrymode_no` = 14;
- -95744159.00

    - JV
        - SELECT SUM(`common_fee_collection_headwises`.`amount`) AS `amount` 
            FROM `common_fee_collection_headwises` 
            INNER JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id` 
            WHERE `common_fee_collections`.`entrymode_no` = 14 
                AND `common_fee_collections`.`inactive` = 0;
        - -789000.00

        - SELECT 
                `temp_data`.`id`, 
                `temp_data`.`date`, 
                `temp_data`.`admno`, 
                `temp_data`.`roll_no`, 
                GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
                `temp_data`.`voucher_type`,
                SUM(`temp_data`.`adjusted_amount`) AS `adjusted_amount`, 
                `common_fee_collections`.`amount` 
            FROM `common_fee_collections` 
            LEFT JOIN `temp_data` 
                ON `temp_data`.`date` = `common_fee_collections`.`paid_date` 
                AND `temp_data`.`admno` = `common_fee_collections`.`admno` 
                AND `temp_data`.`roll_no` = `common_fee_collections`.`rollno` 
                AND `temp_data`.`academic_year` = `common_fee_collections`.`acadamic_year`
            WHERE `common_fee_collections`.`entrymode_no` = 14 
                AND `common_fee_collections`.`inactive` = 0
                AND `temp_data`.`voucher_type` = 'JV'
            GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`roll_no`, `temp_data`.`academic_year` 
            HAVING SUM(`temp_data`.`adjusted_amount`) != `common_fee_collections`.`amount`;

    - REVJV
        - SELECT SUM(`common_fee_collection_headwises`.`amount`) AS `amount` 
            FROM `common_fee_collection_headwises` 
            INNER JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id` 
            WHERE `common_fee_collections`.`entrymode_no` = 14 
                AND `common_fee_collections`.`inactive` = 1;
        - -94955159.00

        - SELECT 
                `temp_data`.`id`, 
                `temp_data`.`date`, 
                `temp_data`.`admno`, 
                `temp_data`.`roll_no`, 
                GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
                SUM(`temp_data`.`adjusted_amount`) AS `adjusted_amount`, 
                `common_fee_collections`.`amount` 
            FROM `common_fee_collections` 
            LEFT JOIN `temp_data` 
                ON `temp_data`.`date` = `common_fee_collections`.`paid_date` 
                AND `temp_data`.`admno` = `common_fee_collections`.`admno` 
                AND `temp_data`.`roll_no` = `common_fee_collections`.`rollno` 
                AND `temp_data`.`academic_year` = `common_fee_collections`.`acadamic_year`
            WHERE `common_fee_collections`.`entrymode_no` = 14 
                AND `common_fee_collections`.`inactive` = 1
                AND `temp_data`.`voucher_type` = 'REVJV'
            GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`roll_no`, `temp_data`.`academic_year` 
            HAVING SUM(`temp_data`.`adjusted_amount`) != `common_fee_collections`.`amount`;

## fund_tranCfer
- SELECT SUM(`fund_tranCfer_amount`) AS `total` FROM `temp_data`;
- SELECT SUM(`amount`) FROM `common_fee_collections` WHERE `entrymode_no` = 1 AND `inactive` IS NULL;
- SELECT SUM(`common_fee_collection_headwises`.`amount`) AS `amount` 
    FROM `common_fee_collection_headwises` 
    INNER JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id` 
    WHERE `common_fee_collections`.`entrymode_no` = 1
        AND `common_fee_collections`.`inactive` IS NULL;
- 31575163.00

- SELECT 
        `temp_data`.`id`, 
        `temp_data`.`date`, 
        `temp_data`.`admno`, 
        `temp_data`.`roll_no`, 
        GROUP_CONCAT(`temp_data`.`fee_head`) AS `fee_heads`, 
        `temp_data`.`voucher_type`,
        SUM(`temp_data`.`fund_tranCfer_amount`) AS `fund_tranCfer_amount`, 
        `common_fee_collections`.`amount` 
    FROM `common_fee_collections` 
    LEFT JOIN `temp_data` 
        ON `temp_data`.`date` = `common_fee_collections`.`paid_date` 
        AND `temp_data`.`admno` = `common_fee_collections`.`admno` 
        AND `temp_data`.`roll_no` = `common_fee_collections`.`rollno` 
        AND `temp_data`.`academic_year` = `common_fee_collections`.`acadamic_year`
    WHERE `common_fee_collections`.`entrymode_no` = 1 
        AND `common_fee_collections`.`inactive` IS NULL
        AND `temp_data`.`voucher_type` IN ('FUNDTRANSFER', 'Fundtransfer')
    GROUP BY `temp_data`.`date`, `temp_data`.`admno`, `temp_data`.`roll_no`, `temp_data`.`academic_year` 
    HAVING SUM(`temp_data`.`fund_tranCfer_amount`) != `common_fee_collections`.`amount`;


## Extra
- SELECT 
        `financial_tran_details`.`temp_id`, 
        `financial_tran_details`.`financial_tran_id`, 
        SUM(`financial_tran_details`.`amount`) AS `amount`, 
        `financial_trans`.`amount` AS `total`
    FROM `financial_tran_details` 
    JOIN `financial_trans` ON `financial_trans`.`id` = `financial_tran_details`.`financial_tran_id`
    GROUP BY `financial_tran_details`.`financial_tran_id` 
    HAVING SUM(`financial_tran_details`.`amount`) != `financial_trans`.`amount`;


- SELECT 
        `common_fee_collection_headwises`.`temp_id`, 
        `common_fee_collection_headwises`.`receipt_id`, 
        SUM(`common_fee_collection_headwises`.`amount`) AS `amount`, 
        `common_fee_collections`.`amount` AS `total`  
    FROM `common_fee_collection_headwises` 
    JOIN `common_fee_collections` ON `common_fee_collections`.`id` = `common_fee_collection_headwises`.`receipt_id`
    GROUP BY `common_fee_collection_headwises`.`receipt_id` 
    HAVING SUM(`common_fee_collection_headwises`.`amount`) != `common_fee_collections`.`amount`;



SELECT
    SUM(due_amount) AS `due_amount1`,
    SUM(paid_amount) AS `paid_amount1`,
    SUM(concession_amount) AS `concession_amount1`,
    SUM(scholarship_amount) AS `scholarship_amount1`,
    SUM(reverse_concession_amount) AS `reverse_concession_amount1`,
    SUM(write_off_amount) AS `write_off_amount1`,
    SUM(adjusted_amount) AS `adjusted_amount1`,
    SUM(refund_amount) AS `refund_amount1`,
    SUM(fund_tranCfer_amount) AS `fund_tranCfer_amount1` 
FROM `temp_data`;

12654422921.00
11461021901.40
90544480.00
471818093.00
496960.00
784403968.60
-95744159.00
-173381473.00
31575163.00

SELECT SUM(
    due_amount + paid_amount + concession_amount + 
    scholarship_amount + reverse_concession_amount + 
    write_off_amount + adjusted_amount + refund_amount + fund_tranCfer_amount
) AS `total` FROM `temp_data`;
25225157855

SELECT SUM(`amount`) AS `total` FROM `financial_tran_details`;
SELECT SUM(`amount`) AS `total` FROM `common_fee_collection_headwises`;

14001686422.60
11223471432.40


C 115922
d 116975

temp_id
365408,366470,366537