<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('common_fee_collection_headwises', function (Blueprint $table) {
            $table->id();
            $table->integer('temp_id');
            $table->integer('receipt_id');
            $table->integer('branche_id');
            $table->integer('module_id');
            $table->integer('fee_type_id');
            $table->string('head_name')->nullable();
            $table->decimal('amount', 64, 2)->default(0);
            // $table->decimal('amount_to_pay', 64, 2)->default(0);
            // $table->decimal('amount_to_percent', 64, 2)->default(0);
            // $table->string('customer_currency')->nullable();
            // $table->string('customer_currency_rate')->nullable();
            // $table->string('base_head_amount')->nullable();
            // $table->string('base_tex_amount')->nullable();
            // $table->string('bse_amount_to_pay')->nullable();
            // $table->string('tex_amount')->nullable();
            // $table->string('prev_head_id')->nullable();
            // $table->string('prev_head_name')->nullable();
            // $table->string('due_ref_no')->nullable();
            // $table->tinyInteger('is_legacy_date')->default(0)->comment('0:no 1:yes');
            // $table->tinyInteger('is_legacy_date_inactive')->default(0)->comment('0:no 1:yes');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('common_fee_collection_headwises');
    }
};
