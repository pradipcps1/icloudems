<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fee_types', function (Blueprint $table) {
            $table->id();
            $table->integer('branche_id');
            $table->integer('fee_categorie_id');
            $table->integer('fee_collection_type_id');
            $table->integer('module_id');
            $table->integer('module_no');
            $table->string('name');
            $table->string('description');
            $table->integer('seq_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fee_types');
    }
};
