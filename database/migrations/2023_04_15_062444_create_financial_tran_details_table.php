<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('financial_tran_details', function (Blueprint $table) {
            $table->id();
            $table->integer('temp_id');
            $table->integer('branche_id');
            $table->integer('fee_type_id');
            $table->integer('financial_tran_id');
            $table->integer('module_id');
            $table->decimal('amount', 64, 2)->default(0);
            $table->string('crdr');
            $table->string('head_name');
            // $table->integer('tran_referance_id');
            // $table->integer('old_trans_id');
            // $table->tinyInteger('is_taxable')->default(0)->comment('0:no 1:yes');
            // $table->integer('total_tax_per');
            // $table->integer('tax_group_id');
            // $table->integer('tax_type_id');
            // $table->integer('head_type');
            // $table->string('customer_currency')->nullable();
            // $table->string('customer_currency_rate')->nullable();
            // $table->string('base_head_amount')->nullable();
            // $table->string('base_tex_amount')->nullable();
            // $table->string('tex_amount')->nullable();
            // $table->string('is_legacy_date')->nullable();
            // $table->tinyInteger('inactive')->default(0)->comment('0:no 1:yes');
            // $table->tinyInteger('fine_due_inactive')->default(0)->comment('0:no 1:yes');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('financial_tran_details');
    }
};
