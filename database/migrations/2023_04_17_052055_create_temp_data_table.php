<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('temp_data', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->string('academic_year');
            $table->string('session');
            $table->string('alloted_category')->nullable();
            $table->string('voucher_type');
            $table->string('voucher_no');
            $table->string('roll_no')->nullable();
            $table->string('admno')->nullable();
            $table->string('status')->nullable();
            $table->string('fee_category')->nullable();
            $table->string('faculty')->nullable();
            $table->string('program')->nullable();
            $table->string('department')->nullable();
            $table->string('batch')->nullable();
            $table->string('receipt_no')->nullable();
            $table->string('fee_head');
            $table->decimal('due_amount', 64, 2)->default(0);
            $table->decimal('paid_amount', 64, 2)->default(0);
            $table->decimal('concession_amount', 64, 2)->default(0);
            $table->decimal('scholarship_amount', 64, 2)->default(0);
            $table->decimal('reverse_concession_amount', 64, 2)->default(0);
            $table->decimal('write_off_amount', 64, 2)->default(0);
            $table->decimal('adjusted_amount', 64, 2)->default(0);
            $table->decimal('refund_amount', 64, 2)->default(0);
            $table->decimal('fund_tranCfer_amount', 64, 2)->default(0);
            $table->text('remarks')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('temp_data');
    }
};
