<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('common_fee_collections', function (Blueprint $table) {
            $table->id();
            $table->integer('branche_id');
            $table->integer('module_id');
            $table->string('tran_id')->nullable();
            $table->integer('entrymode_id');
            $table->string('entrymode_no')->nullable();
            $table->string('admno')->nullable();
            $table->string('rollno')->nullable();
            $table->decimal('amount', 64, 2)->default(0);
            $table->string('acadamic_year');
            $table->string('financial_year');
            $table->string('display_receipt_no')->nullable();
            $table->date('paid_date');
            $table->integer('inactive')->nullable()->comment('0:no 1:yes');
            // $table->decimal('amount_to_pay', 64, 2)->default(0);
            // $table->string('pay_mode')->nullable();
            // $table->string('created_by')->nullable();
            // $table->string('remark')->nullable();
            // $table->string('stu_class')->nullable();
            // $table->string('fee_category')->nullable();
            // $table->string('stu_status')->nullable();
            // $table->string('erp_sync')->nullable();
            // $table->string('rev_rep_sync')->nullable();
            // $table->string('cancelled_by')->nullable();
            // $table->string('cancelled_date')->nullable();
            // $table->string('cancelled_date_time')->nullable();
            // $table->string('cancelled_remark')->nullable();
            // $table->string('ddno')->nullable();
            // $table->string('dd_date')->nullable();
            // $table->string('dd_bank')->nullable();
            // $table->string('client_bank')->nullable();
            // $table->string('challan_refid')->nullable();
            // $table->string('adjust_receipt_id')->nullable();
            // $table->string('due_ref_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('common_fee_collections');
    }
};
