<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('financial_trans', function (Blueprint $table) {
            $table->id();
            $table->integer('branche_id');
            $table->integer('module_id');
            $table->integer('entrymode_id');
            $table->string('entrymode_no')->nullable();
            $table->integer('fee_type_id');
            $table->string('admno')->nullable();
            $table->decimal('amount', 64, 2)->default(0);
            $table->string('crdr');
            $table->date('tran_date');
            $table->string('acad_year');
            $table->string('voucher_no');
            $table->string('voucher_type');
            $table->integer('type_concession')->nullable()->comment('0:no 1:yes');
            // $table->tinyInteger('account_type')->default(1)->comment('0:no 1:student');
            // $table->integer('account_id');
            // $table->dateTime('creates_date')->nullable();
            // $table->dateTime('updated_date')->nullable();
            // $table->string('is_chalan')->nullable();
            // $table->string('chalan_no')->nullable();
            // $table->date('chalan_date')->nullable();
            // $table->string('chalan_gen_by')->nullable();
            // $table->string('tran_type')->nullable();
            // $table->string('tran_on_bank')->nullable();
            // $table->string('tran_ref_no')->nullable();
            // $table->date('tran_ref_date')->nullable();
            // $table->tinyInteger('inactive')->default(0)->comment('0:no 1:yes');
            // $table->string('payment_rec_by')->nullable();
            // $table->string('is_payment_rec')->nullable();
            // $table->string('is_pay_autorized')->nullable();
            // $table->string('pay_autorized_by')->nullable();
            // $table->string('isreconciled')->nullable();
            // $table->string('reconciled_by')->nullable();
            // $table->string('remark')->nullable();
            // $table->string('member_class_id')->nullable();
            // $table->string('fee_category')->nullable();
            // $table->string('member_status')->nullable();
            // $table->string('erp_sync')->nullable();
            // $table->string('entry_mode')->nullable();
            // $table->string('adjust_from')->nullable();
            // $table->string('adjust_receipt_no')->nullable();
            // $table->string('adjust_amount')->nullable();
            // $table->string('installment_no')->nullable();
            // $table->string('currency')->nullable();
            // $table->string('created_by')->nullable();
            // $table->string('is_individual_alter')->nullable();
            // $table->tinyInteger('status')->default(0)->comment('0:no 1:yes');
            // $table->string('concession_type')->nullable();
            // $table->string('misc_due_id')->nullable();
            // $table->string('dispaly_invoice_no')->nullable();
            // $table->string('invoice_against_admno')->nullable();
            // $table->string('invoice_against_invoice')->nullable();
            // $table->string('customer_currency')->nullable();
            // $table->string('customer_currency_rate')->nullable();
            // $table->string('base_amount')->nullable();
            // $table->string('due_ref_no')->nullable();
            // $table->string('file_name')->nullable();
            // $table->string('hostel_id')->nullable();
            // $table->string('hostel_join_date')->nullable();
            // $table->string('route_id')->nullable();
            // $table->string('trp_join_date')->nullable();
            // $table->string('br_id')->nullable();
            // $table->string('manually_created')->nullable();
            // $table->string('session')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('financial_trans');
    }
};
