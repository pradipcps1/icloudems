<?php

namespace App\Console\Commands;

use App\Models\Branche;
use App\Models\CommonFeeCollection;
use App\Models\Entrymode;
use App\Models\FinancialTrans;
use App\Models\Module;
use App\Models\TempData;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TempImportFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:temp-import-fee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $branches = Branche::get();
        $modules = Module::get();

        $from = 1;
        $to = 10000;
        for ($i = 0; $i < 89; $i++) {
            $start = now();
            $this->start($from, $to, $branches, $modules);
            $time = $start->diffInSeconds(now());
            dump($from . ' to ' . $to . " seconds $time");
            $from += 10000;
            $to += 10000;
        }
    }

    public function start($from, $to, $branches, $modules)
    {
        $financialId = FinancialTrans::count() - 3000;
        $commonId = CommonFeeCollection::count() - 3000;
        $tempDatas = TempData::whereBetween('id', [$from, $to])->get();
        foreach ($tempDatas as $data) {
            $detail = $this->detailGet($data);
            $branche = $branches->where('branch_name', $data->department)->first();
            $module = $modules->where('module_no', $detail['module_no'])->first();

            if ($detail['type'] == 0) {
                $financialTrana = FinancialTrans::where('id', '>', $financialId)
                    ->where('entrymode_no', $detail['entrymode_no'])
                    ->where('admno', $data->admno)
                    ->where('tran_date', $data->date)
                    ->where('acad_year', $data->academic_year);
                if (in_array($detail['inactive'], [0, 1])) {
                    $financialTrana = $financialTrana->where('type_concession', $detail['type_concession']);
                } else {
                    $financialTrana = $financialTrana->whereNull('type_concession');
                }
                $financialTrans = $financialTrana->first();
                if (!$financialTrans) {
                    $financialTrans = FinancialTrans::create([
                        'branche_id' => $branche->id,
                        'module_id' => $module->id,
                        'entrymode_id' => 0,
                        'entrymode_no' => $detail['entrymode_no'],
                        'fee_type_id' => 0,
                        'admno' => $data->admno,
                        'amount' => $detail['amount'],
                        'crdr' => $detail['crdr'],
                        'tran_date' => $data->date,
                        'acad_year' => $data->academic_year,
                        'voucher_no' => $data->voucher_no,
                        'voucher_type' => $data->voucher_type,
                        'type_concession' => $detail['type_concession'],
                    ]);
                    $financialId++;
                } else {
                    $financialTrans->amount += $detail['amount'];
                    $financialTrans->save();
                }

                DB::table('financial_tran_details')->insert([
                    'temp_id' => $data->id,
                    'branche_id' => $branche->id,
                    'fee_type_id' => 0,
                    'financial_tran_id' => $financialTrans->id,
                    'module_id' => $module->id,
                    'amount' => $detail['amount'],
                    'crdr' => $detail['crdr'],
                    'head_name' => $data->fee_head,
                ]);
            } else {
                $commonFeeCollectiona = CommonFeeCollection::where('id', '>', $commonId)
                    ->where('entrymode_no', $detail['entrymode_no'])
                    ->where('admno', $data->admno)
                    ->where('rollno', $data->roll_no)
                    ->where('acadamic_year', $data->academic_year)
                    ->where('paid_date', $data->date);

                if (in_array($detail['inactive'], [0, 1])) {
                    $commonFeeCollectiona = $commonFeeCollectiona->where('inactive', $detail['inactive']);
                } else {
                    $commonFeeCollectiona = $commonFeeCollectiona->whereNull('inactive');
                }
                $commonFeeCollection = $commonFeeCollectiona->first();
                if (!$commonFeeCollection) {
                    $commonFeeCollection = CommonFeeCollection::create([
                        'branche_id' => $branche->id,
                        'module_id' => $module->id,
                        'tran_id' => $data->receipt_no,
                        'entrymode_id' => 0,
                        'entrymode_no' => $detail['entrymode_no'],
                        'admno' => $data->admno,
                        'rollno' => $data->roll_no,
                        'amount' => $detail['amount'],
                        'acadamic_year' => $data->academic_year,
                        'financial_year' => $data->academic_year,
                        'display_receipt_no' => $data->receipt_no,
                        'paid_date' => $data->date,
                        'inactive' => $detail['inactive'],
                    ]);
                    $commonId++;
                } else {
                    $commonFeeCollection->amount += $detail['amount'];
                    $commonFeeCollection->save();
                }

                DB::table('common_fee_collection_headwises')->insert([
                    'temp_id' => $data->id,
                    'branche_id' => $branche->id,
                    'receipt_id' => $commonFeeCollection->id,
                    'module_id' => $module->id,
                    'fee_type_id' => 0,
                    'head_name' => $data->fee_head,
                    'amount' => $detail['amount'],
                ]);
            }
        }
    }

    public function detailGet($data)
    {
        $type = 1;
        $amount = 0;
        $name = $data->voucher_type;
        $crdr = '';
        $entrymode_no = '';
        $inactive = null;
        $type_concession = null;
        if ($data->voucher_type == 'DUE') {
            $type = 0;
            $amount = $data->due_amount;
            $crdr = 'D';
            $entrymode_no = '0';
        } else if ($data->voucher_type == 'REVDUE') {
            $type = 0;
            $amount = $data->write_off_amount;
            $crdr = 'C';
            $entrymode_no = '12';
        } else if ($data->voucher_type == 'SCHOLARSHIP') {
            $type = 0;
            $amount = $data->scholarship_amount;
            $crdr = 'C';
            $entrymode_no = '15';
            $type_concession = 2;
        } else if (in_array($data->voucher_type, ['REVSCHOLARSHIP', 'REVCONCESSION'])) {
            $type = 0;
            $amount = $data->reverse_concession_amount;
            $name = 'REVSCHOLARSHIP/REVCONCESSION';
            $crdr = 'D';
            $entrymode_no = '16';
        } else if ($data->voucher_type == 'CONCESSION') {
            $type = 0;
            $amount = $data->concession_amount;
            $crdr = 'C';
            $entrymode_no = '15';
            $type_concession = 1;
        } else if ($data->voucher_type == 'RCPT') {
            $amount = $data->paid_amount;
            $crdr = 'C';
            $entrymode_no = '0';
            $inactive = 0;
        } else if ($data->voucher_type == 'REVRCPT') {
            $amount = $data->paid_amount;
            $crdr = 'D';
            $entrymode_no = '0';
            $inactive = 1;
        } else if ($data->voucher_type == 'JV') {
            $amount = $data->adjusted_amount;
            $crdr = 'C';
            $entrymode_no = '14';
            $inactive = 0;
        } else if ($data->voucher_type == 'REVJV') {
            $amount = $data->adjusted_amount;
            $crdr = 'D';
            $entrymode_no = '14';
            $inactive = 1;
        } else if ($data->voucher_type == 'PMT') {
            $amount = $data->refund_amount;
            $crdr = 'D';
            $entrymode_no = '1';
            $inactive = 0;
        } else if ($data->voucher_type == 'REVPMT') {
            $amount = $data->refund_amount;
            $crdr = 'C';
            $entrymode_no = '1';
            $inactive = 1;
        } else if (in_array($data->voucher_type, ['FUNDTRANSFER', 'Fundtransfer'])) {
            $amount = $data->fund_tranCfer_amount;
            $crdr = 'positive and nagative';
            $entrymode_no = '1';
        }

        $module = '';
        $module_no = '';

        if (in_array($data->fee_head, ['Fine Fee'])) {
            $module = 'AcademicMisc';
            $module_no = '11';
        } else if (in_array($data->fee_head, ['TUITION FEE'])) {
            $module = 'Hostel';
            $module_no = '2';
        } else if (in_array($data->fee_head, ['Tuition Fee (Back Paper)'])) {
            $module = 'HostelMisc';
            $module_no = '22';
        } else if (in_array($data->fee_head, ['Adjustable Excess Fee'])) {
            $module = 'TransportMISC';
            $module_no = '33';
        } else if (in_array($data->fee_head, ['Adjusted_Amount', 'Ajustable_Excess_Amount'])) {
            $module = 'Transport';
            $module_no = '3';
        } else {
            $module = 'Academic';
            $module_no = '1';
        }

        return [
            'type' => $type,
            'amount' => $amount,
            'name' => $name,
            'crdr' => $crdr,
            'entrymode_no' => $entrymode_no,
            'inactive' => $inactive,
            'type_concession' => $type_concession,
            'module' => $module,
            'module_no' => $module_no,
        ];
    }
}
