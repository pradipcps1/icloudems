<?php

namespace App\Console\Commands;

use App\Imports\BulkImport;
use Carbon\Carbon;
use Excel;
use Illuminate\Console\Command;

class ZipImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:zip-import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $start = now();
        Excel::import(new BulkImport, storage_path("csv/Bulk_Ledger_04_03_2021_17_13_28.csv"));
        $time = $start->diffInSeconds(now());
        dump("Processed in $time seconds");
        return Command::SUCCESS;
    }
}
