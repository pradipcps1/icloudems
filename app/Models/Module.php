<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'module', 'module_no',
    ];

    public function feeType()
    {
        return $this->hasMany(FeeType::class);
    }

    public function financialTrans()
    {
        return $this->hasMany(FinancialTrans::class);
    }

    public function financialTranDetails()
    {
        return $this->hasMany(FinancialTranDetails::class);
    }

    public function commonFeeCollection()
    {
        return $this->hasMany(CommonFeeCollection::class);
    }

    public function commonFeeCollectionHeadwise()
    {
        return $this->hasMany(CommonFeeCollectionHeadwise::class);
    }
}
