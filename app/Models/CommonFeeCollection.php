<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommonFeeCollection extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'branche_id',
        'module_id',
        'tran_id',
        'entrymode_id',
        'entrymode_no',
        'admno',
        'rollno',
        'amount',
        'acadamic_year',
        'financial_year',
        'display_receipt_no',
        'paid_date',
        'inactive',
    ];

    public function branche()
    {
        return $this->belongsTo(Branche::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function entrymode()
    {
        return $this->belongsTo(Entrymode::class);
    }

    public function headwise()
    {
        return $this->hasMany(CommonFeeCollectionHeadwise::class, 'receipt_id');
    }
}
