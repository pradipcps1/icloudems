<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entrymode extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'name', 'crdr', 'entrymode_no',
    ];

    public function financialTrans()
    {
        return $this->hasMany(FinancialTrans::class);
    }

    public function commonFeeCollection()
    {
        return $this->hasMany(CommonFeeCollection::class);
    }
}
