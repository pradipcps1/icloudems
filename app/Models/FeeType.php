<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeeType extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'branche_id', 'fee_categorie_id', 'fee_collection_type_id', 'module_id', 'module_no', 'name', 'description', 'seq_id',
    ];

    public function branche()
    {
        return $this->belongsTo(Branche::class);
    }

    public function feeCategory()
    {
        return $this->belongsTo(FeeCategory::class);
    }

    public function feeCollectionType()
    {
        return $this->belongsTo(FeeCollectionType::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function financialTrans()
    {
        return $this->hasMany(FinancialTrans::class);
    }

    public function financialTranDetails()
    {
        return $this->hasMany(FinancialTranDetails::class);
    }

    public function commonFeeCollectionHeadwise()
    {
        return $this->hasMany(CommonFeeCollectionHeadwise::class);
    }

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($data) {
            $count = FeeType::where('branche_id', $data->branche_id)
                ->where('fee_categorie_id', $data->fee_categorie_id)
                ->where('fee_collection_type_id', $data->fee_collection_type_id)
                ->where('module_id', $data->module_id)
                ->count();
            $data->seq_id = $count + 1;
        });
    }
}
