<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommonFeeCollectionHeadwise extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'temp_id', 
        'receipt_id',
        'branche_id',
        'module_id',
        'fee_type_id',
        'head_name',
        'amount',
    ];

    public function commonFeeCollection()
    {
        return $this->belongsTo(CommonFeeCollection::class);
    }

    public function branche()
    {
        return $this->belongsTo(Branche::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function feeType()
    {
        return $this->belongsTo(FeeType::class);
    }
}
