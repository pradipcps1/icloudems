<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialTrans extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'branche_id', 'module_id', 'entrymode_id', 'entrymode_no', 'fee_type_id', 'admno', 'amount', 'crdr', 'tran_date', 'acad_year', 'voucher_no', 'voucher_type', 'type_concession',
    ];

    public function branche()
    {
        return $this->belongsTo(Branche::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function entrymode()
    {
        return $this->belongsTo(Entrymode::class);
    }

    public function feeType()
    {
        return $this->belongsTo(FeeType::class);
    }

    public function details()
    {
        return $this->hasMany(FinancialTranDetails::class);
    }
}
