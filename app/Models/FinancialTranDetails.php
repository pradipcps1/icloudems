<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialTranDetails extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'temp_id', 'branche_id', 'fee_type_id', 'financial_tran_id', 'module_id', 'amount', 'crdr', 'head_name',
    ];

    public function branche()
    {
        return $this->belongsTo(Branche::class);
    }

    public function feeType()
    {
        return $this->belongsTo(FeeType::class);
    }

    public function financialTrans()
    {
        return $this->belongsTo(FinancialTrans::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
