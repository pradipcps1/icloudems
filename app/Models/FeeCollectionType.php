<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeeCollectionType extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'branche_id', 'collectionhead', 'collectiondesc',
    ];

    public function branche()
    {
        return $this->belongsTo(Branche::class);
    }

    public function feeType()
    {
        return $this->hasMany(FeeType::class);
    }
}
