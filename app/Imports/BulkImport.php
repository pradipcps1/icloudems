<?php

namespace App\Imports;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;

class BulkImport implements ToModel, WithStartRow, WithChunkReading
{
    public function model(array $row)
    {
        $date = Carbon::parse($row[1])->format('Y-m-d');
        DB::table('temp_data')->insert([
            'date' => $date,
            'academic_year' => $row[2],
            'session' => $row[3],
            'alloted_category' => $row[4],
            'voucher_type' => $row[5],
            'voucher_no' => $row[6],
            'roll_no' => $row[7],
            'admno' => $row[8],
            'status' => $row[9],
            'fee_category' => $row[10],
            'faculty' => $row[11],
            'program' => $row[12],
            'department' => $row[13],
            'batch' => $row[14],
            'receipt_no' => $row[15],
            'fee_head' => $row[16],
            'due_amount' => $row[17],
            'paid_amount' => $row[18],
            'concession_amount' => $row[19],
            'scholarship_amount' => $row[20],
            'reverse_concession_amount' => $row[21],
            'write_off_amount' => $row[22],
            'adjusted_amount' => $row[23],
            'refund_amount' => $row[24],
            'fund_tranCfer_amount' => $row[25],
            'remarks' => $row[26],
        ]);
    }

    public function startRow(): int
    {
        return 7;
    }

    public function chunkSize(): int
    {
        return 5000;
    }
}
